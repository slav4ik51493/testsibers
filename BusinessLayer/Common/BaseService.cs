﻿using BusinessLayer.Interfaces;
using DataLayer.Interfaces;
using System.Collections.Generic;
using AutoMapper;
using System.Linq;
using BusinessLayer.Entities;
using DataLayer.Entities;

namespace BusinessLayer.Common
{
    public abstract class BaseService<T, D> : IBaseService<T> where T : BaseDTO where D : Base
    {
        protected readonly IBaseRepository<D> _repository;
        protected readonly IMapper _mapper;
        public BaseService(IBaseRepository<D> repository)
        {
            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<D, T>();
                cfg.CreateMap<T, D>();
            });
            _mapper = config.CreateMapper();

            _repository = repository;
        }

        public void Add(T item)
        {
            _repository.Add(_mapper.Map<T, D>(item));
        }

        public void Delete(T item)
        {
            _repository.Delete(_mapper.Map<T, D>(item));
        }

        public T Find(int id)
        {
            return _mapper.Map<D, T>(_repository.Find(id));
        }

        public List<T> GetAll()
        {
            return _mapper
                .Map<IQueryable<D>, IQueryable<T>>(_repository.GetAll())
                .ToList();
        }

        public void Update(T item)
        {
            _repository.Update(_mapper.Map<T, D>(item));
        }
    }
}
