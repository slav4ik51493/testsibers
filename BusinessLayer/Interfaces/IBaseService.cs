﻿using BusinessLayer.Entities;
using System.Collections.Generic;

namespace BusinessLayer.Interfaces
{
    public interface IBaseService<T> where T : BaseDTO
    {
        List<T> GetAll();
        T Find(int id);
        void Add(T item);
        void Update(T item);
        void Delete(T item);
    }
}
