﻿using BusinessLayer.Entities;

namespace BusinessLayer.Interfaces
{
    public interface IProjectService : IBaseService<ProjectDTO>
    {
    }
}
