﻿using BusinessLayer.Entities;

namespace BusinessLayer.Interfaces
{
    public interface IEmployeeService : IBaseService<EmployeeDTO>
    {
    }
}
