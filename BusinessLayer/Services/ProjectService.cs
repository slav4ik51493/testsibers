﻿using BusinessLayer.Common;
using BusinessLayer.Entities;
using BusinessLayer.Interfaces;
using DataLayer.Entities;
using DataLayer.Interfaces;

namespace BusinessLayer.Services
{
    public class ProjectService : BaseService<ProjectDTO, Project>, IProjectService
    {
        public ProjectService(IBaseRepository<Project> repository) : base(repository)
        {
        }
    }
}
