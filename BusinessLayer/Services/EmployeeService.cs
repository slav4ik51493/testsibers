﻿using BusinessLayer.Common;
using BusinessLayer.Entities;
using BusinessLayer.Interfaces;
using DataLayer.Entities;
using DataLayer.Interfaces;

namespace BusinessLayer.Services
{
    public class EmployeeService : BaseService<EmployeeDTO, Employee>, IEmployeeService
    {
        public EmployeeService(IBaseRepository<Employee> repository) : base(repository)
        {
        }
    }
}
