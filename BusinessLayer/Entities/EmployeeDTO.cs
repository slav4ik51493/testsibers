﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BusinessLayer.Entities
{
    public class EmployeeDTO : BaseDTO
    {
        public string FirstName { get; set; }
        public string Surname { get; set; }
        public string Patronymic { get; set; }
        public string Email { get; set; }
    }
}
