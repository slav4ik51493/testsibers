﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BusinessLayer.Entities
{
    public class ProjectDTO : BaseDTO
    {
        public string Name { get; set; }
        public string CompanyCustomer { get; set; }
        public string CompanyPerformer { get; set; }
        public int Priority { get; set; }

        public int? LeaderId { get; set; }
        public virtual EmployeeDTO Leader { get; set; }

        public DateTime DateStart { get; set; }
        public DateTime DateFinish { get; set; }
    }
}
