﻿using DataLayer.Entities;

namespace DataLayer.Interfaces
{
    public interface IEmployeeRepository: IBaseRepository<Employee>
    {
    }
}
