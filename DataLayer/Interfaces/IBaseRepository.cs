﻿using DataLayer.Entities;
using System.Linq;

namespace DataLayer.Interfaces
{
    public interface IBaseRepository<T> where T : Base
    {
        IQueryable<T> GetAll();
        T Find(int id);
        void Add(T item);
        void Update(T item);
        void Delete(T item);
    }
}
