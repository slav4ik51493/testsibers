﻿using DataLayer.Entities;

namespace DataLayer.Interfaces
{
    public interface IProjectRepository: IBaseRepository<Project>
    {
    }
}
