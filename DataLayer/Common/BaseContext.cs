﻿using DataLayer.Entities;
using Microsoft.EntityFrameworkCore;

namespace DataLayer.Common
{
    public class BaseContext : DbContext
    {
        public DbSet<Employee> Employees { get; set; }
        public DbSet<Project> Projects { get; set; }

        public BaseContext(DbContextOptions options)
            : base(options)
        { }
    }
}