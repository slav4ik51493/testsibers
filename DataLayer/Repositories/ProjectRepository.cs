﻿using DataLayer.Common;
using DataLayer.Entities;
using DataLayer.Interfaces;

namespace DataLayer.Repositories
{
    class ProjectRepository : BaseRepository<Project>, IProjectRepository
    {
        public ProjectRepository(BaseContext context) : base(context)
        {
        }
    }
}
