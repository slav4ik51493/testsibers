﻿using System.ComponentModel.DataAnnotations;

namespace DataLayer.Entities
{
    public class Base
    {
        [Key]
        public int Id { get; set; }
    }
}
