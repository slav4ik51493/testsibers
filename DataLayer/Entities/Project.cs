﻿using System;
using System.Collections.Generic;

namespace DataLayer.Entities
{
    public class Project : Base
    {
        public string Name { get; set; }
        public string CompanyCustomer { get; set; }
        public string CompanyPerformer { get; set; }
        public int Priority { get; set; }

        public int? LeaderId { get; set; }
        public virtual Employee Leader { get; set; }

        public DateTime DateStart { get; set; }
        public DateTime DateFinish { get; set; }   

        public ICollection<Employee> Employees { get; set; }
    }
}
